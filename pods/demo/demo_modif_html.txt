## Demo avec une modification de la page web html : 

# créer 1 pods : 

apiVersion: v1
kind: Pod
metadata: 
  name: httpd
spec: 
  containers: 
  - name: httpd
    image: httpd:latest

# rentrer dans un pod :

kubectl exec -i -t httpd -- /bin/bash

echo "<html><h1>Coucou</h1></html>" > htdocs/index.html

kubectl port-forward httpd 8081:80 